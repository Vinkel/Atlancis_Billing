<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:56 PM
 */

require_once('includes/header.php');

?>

    <!-- MAIN CONTENT-->
    <div class="main-content">

        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <!-- Resource Usage -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1"><i class="far fa-credit-card"></i> Payment Methods List</h2>
                            <!--<button class="au-btn au-btn-icon au-btn&#45;&#45;blue">
                                <i class="zmdi zmdi-plus"></i>add item</button>-->
                        </div>
                    </div>

                    <div class="col-md-12">

                        <div class="au-task__footer">
                            <a class="pull-left" href="add-credit-card.php"><button class="au-btn au-btn--blue2 js-load-btn"><i class="fas fa-plus-circle"></i> Add Credit Card</button></a>
                        </div>
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table class="table table-borderless table-hover table-data2">
                                <thead>
                                <tr>
                                    <th>Card Holder's Name</th>
                                    <th>Card Number</th>
                                    <th>Expiry Date</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>iPhone X 64Gb Grey</td>
                                    <td class="process">Processed</td>
                                    <td>2018-09-29 05:57</td>
                                    <td>Mobile</td>
                                    <td>$999.00</td>
                                </tr>
                                <tr>
                                    <td>Samsung S8 Black</td>
                                    <td class="process">Processed</td>
                                    <td>2018-09-28 01:22</td>
                                    <td>Mobile</td>
                                    <td>$756.00</td>
                                </tr>
                                <tr>
                                    <td>Game Console Controller</td>
                                    <td class="denied">Denied</td>
                                    <td>2018-09-27 02:12</td>
                                    <td>Game</td>
                                    <td>$22.00</td>
                                </tr>
                                <tr>
                                    <td>iPhone X 256Gb Black</td>
                                    <td class="denied">Denied</td>
                                    <td>2018-09-26 23:06</td>
                                    <td>Mobile</td>
                                    <td>$1199.00</td>
                                </tr>
                                <tr>
                                    <td>USB 3.0 Cable</td>
                                    <td class="process">Processed</td>
                                    <td>2018-09-25 19:03</td>
                                    <td>Accessories</td>
                                    <td>$10.00</td>
                                </tr>
                                <tr>
                                    <td>Smartwatch 4.0 LTE Wifi</td>
                                    <td class="denied">Denied</td>
                                    <td>2018-09-29 05:57</td>
                                    <td>Accesories</td>
                                    <td>$199.00</td>
                                </tr>
                                <tr>
                                    <td>Camera C430W 4k</td>
                                    <td class="process">Processed</td>
                                    <td>2018-09-24 19:10</td>
                                    <td>Camera</td>
                                    <td>$699.00</td>
                                </tr>
                                <tr>
                                    <td>Macbook Pro Retina 2017</td>
                                    <td class="process">Processed</td>
                                    <td>2018-09-22 00:43</td>
                                    <td>Computer</td>
                                    <td>$10.00</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>

                <?php require_once('includes/foot.php'); ?>

            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->

<?php require_once('includes/footer.php'); ?>