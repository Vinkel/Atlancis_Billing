<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:56 PM
 */

require_once('includes/header.php');

?>

    <!-- MAIN CONTENT-->
    <div class="main-content">

        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <!-- Resource Usage -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1"><i class="fas fa-clipboard"></i> Order Information</h2>
                            <!--<button class="au-btn au-btn-icon au-btn&#45;&#45;blue">
                                <i class="zmdi zmdi-plus"></i>add item</button>-->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1"><i
                                                        class="fas fa-cart-plus"></i> Order #09977897979879</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Initialization Date:</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">2018-08-27 07:08</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Last Payment Date:</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">2018-08-27 07:08</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1"><i
                                                        class="fas fa-map-marker-alt"></i> Address and Contact
                                                Information</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">First Name</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">First Name</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Last Name</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Last Name</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Company</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Company Name</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Address</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Address</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Phone</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Phone Number</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Email</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Email Address</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Invoice Send Date</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Date</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Charge Retry Interval</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Monthly/Annualy</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1"><i
                                                        class="fas fa-info-circle"></i> Order Information</label>
                                        </div>
                                    </div>
                                    <!-- DATA TABLE-->
                                    <div class="table-responsive table-hover m-b-40">
                                        <table class="table table-borderless table-data2">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Service</th>
                                                <th>Setup Date</th>
                                                <th>Type</th>
                                                <th>Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td class="process">Processed</td>
                                                <td>2018-09-29 05:57</td>
                                                <td>Mobile</td>
                                                <td>$999.00</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td class="process">Processed</td>
                                                <td>2018-09-28 01:22</td>
                                                <td>Mobile</td>
                                                <td>$756.00</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td class="denied">Denied</td>
                                                <td>2018-09-27 02:12</td>
                                                <td>Game</td>
                                                <td>$22.00</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td class="denied">Denied</td>
                                                <td>2018-09-26 23:06</td>
                                                <td>Mobile</td>
                                                <td>$1199.00</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td class="process">Processed</td>
                                                <td>2018-09-25 19:03</td>
                                                <td>Accessories</td>
                                                <td>$10.00</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td class="denied">Denied</td>
                                                <td>2018-09-29 05:57</td>
                                                <td>Accesories</td>
                                                <td>$199.00</td>
                                            </tr>
                                            <tr>
                                                <td>7</td>
                                                <td class="process">Processed</td>
                                                <td>2018-09-24 19:10</td>
                                                <td>Camera</td>
                                                <td>$699.00</td>
                                            </tr>
                                            <tr>
                                                <td>8</td>
                                                <td class="process">Processed</td>
                                                <td>2018-09-22 00:43</td>
                                                <td>Computer</td>
                                                <td>$10.00</td>
                                            </tr>
                                            </tbody>
                                            <tbody>
                                            <tr>
                                                <td></td>
                                                <td class="process"></td>
                                                <td>Total</td>
                                                <td>$4567</td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END DATA TABLE                  -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1"><i
                                                        class="fas fa-book-open"></i> Invoice Information</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Invoice ID</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">10121552</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Generated On:</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">2018-08-27 07:08</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Due On:</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">2018-09-01 00:09</label>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Amount</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">$269</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <?php require_once('includes/foot.php'); ?>

        </div>
    </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->

<?php require_once('includes/footer.php'); ?>