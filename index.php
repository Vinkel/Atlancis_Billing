<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:56 PM
 */

require_once('includes/header.php');

?>

    <!-- MAIN CONTENT-->
    <div class="main-content">

        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <!-- Resource Usage -->
                <!--<div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1">Resource Usage</h2>
                            <!--<button class="au-btn au-btn-icon au-btn&#45;&#45;blue">
                                <i class="zmdi zmdi-plus"></i>add item</button>-
                        </div>
                    </div>
                </div>-->
                <div class="row m-t-25">
                    <div class="col-sm-6 col-lg-3">
                        <div class="overview-item overview-item--c1">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fab fa-cloudversify"></i>
                                    </div>
                                    <div class="text">
                                        <h2>10368</h2>
                                        <span>Virtual Machines</span>
                                    </div>
                                </div>
                                <div class="overview-chart">
                                    <canvas id="widgetChart1"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="overview-item overview-item--c2">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fas fa-tachometer-alt"></i>
                                    </div>
                                    <div class="text">
                                        <h2>388 GB</h2>
                                        <span>Compute Resources</span>
                                    </div>
                                </div>
                                <div class="overview-chart">
                                    <canvas id="widgetChart2"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="overview-item overview-item--c3">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fas fa-chart-line"></i>
                                    </div>
                                    <div class="text">
                                        <h2>1,086</h2>
                                        <span>Volumes</span>
                                    </div>
                                </div>
                                <div class="overview-chart">
                                    <canvas id="widgetChart3"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="overview-item overview-item--c4">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fas fa-chart-pie"></i>
                                    </div>
                                    <div class="text">
                                        <h2>386 GB</h2>
                                        <span>Storage</span>
                                    </div>
                                </div>
                                <div class="overview-chart">
                                    <canvas id="widgetChart4"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Resource Usage -->
                <!-- Service Offerings -->
                <div class="row">
                    <div class="col-lg-12">
                            <div class="au-card-title" style="background-image:url('images/bg-title-01.jpg');">
                                <div class="bg-overlay bg-overlay--blue"></div>
                                <h3>
                                    <i class="fas fa-cloud"></i>Service Offerings</h3>
                            </div>
                        <div class="au-card-inner">
                            <!-- First Row -->
                            <div class="row soffering">
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow-lg mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">Small Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">1</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">1 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">30 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 13.13</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow-lg mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">2nd Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">1</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">2 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">50 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 14.44</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow-lg mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">3rd Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">1</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">3 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">60 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 15.56</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End First Row -->

                            <!-- Second Row -->
                            <div class="row soffering">
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow-lg mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">4th Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">2</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">2 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">60 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 26.25</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">5th Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">3</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">1 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">60 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 36.94</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">6th Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">2</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">4 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">80 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 48.50</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End Second Row -->

                            <!-- Third Row -->
                            <div class="row soffering">
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">7th Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">4</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">8 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">160 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 56.99</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">8th Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">6</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">16 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">320 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 90.75</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="au-card chart-percent-card shadow mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">9th Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">8</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">32 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">640 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 135.00</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End Third Row -->

                            <!-- Fourth Row -->
                            <div class="row soffering">
                                <div class="col-lg-6">
                                    <div class="au-card chart-percent-card shadow mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">10th Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">12</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">48 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">960 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 202.55</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="au-card chart-percent-card shadow mb-5 rounded">
                                        <div class="au-card-inner">
                                            <h3 class="title-2 tm-b-5">Maximum Offering</h3>
                                            <div class="row no-gutters">
                                                <div class="col-xl-6">
                                                    <div class="chart-note-wrap">
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--blue"></span>
                                                            <span>VCPU</span> <span
                                                                    class="badge badge-light title-5">16</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--red"></span>
                                                            <span>RAM</span> <span
                                                                    class="badge badge-light title-5">64 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <span class="dot dot--green"></span>
                                                            <span>Storage</span> <span
                                                                    class="badge badge-light title-5">1280 GB</span>
                                                        </div>
                                                        <div class="chart-note mr-0 d-block">
                                                            <i class="fas fa-money-bill"></i>
                                                            <span>Price</span> <span class="badge badge-danger"><h4
                                                                        class="text-white"><i
                                                                            class="fas fa-dollar-sign"></i> 270.00</h4></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="percent-chart">
                                                        <button type="button" class="btn btn-primary btn-sm price">
                                                            <i class="fas fa-cart-plus"></i> Purchase
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End Fourth Row -->
                        </div>

                    </div>
                </div><!-- End Service Offerings -->

                <?php require_once('includes/foot.php'); ?>

            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->

<?php require_once('includes/footer.php'); ?>