<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:56 PM
 */

require_once('includes/header.php');

?>

    <!-- MAIN CONTENT-->
    <div class="main-content">

        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <!-- Resource Usage -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1"><i class="fas fa-clipboard"></i> Account Information</h2>
                            <!--<button class="au-btn au-btn-icon au-btn&#45;&#45;blue">
                                <i class="zmdi zmdi-plus"></i>add item</button>-->
                        </div>
                    </div>

                    <form id="account" method="post" action="account.php">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong><i class="fas fa-file-contract"></i> Contact Details</strong>
                                </div>
                                <div class="card-body card-block">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="fname" class="control-label mb-1">First Name</label>
                                                <input type="text" name="fname" id="fname"
                                                       placeholder="Enter your first name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="lname" class="control-label mb-1">Last Name</label>
                                            <div class="input-group">
                                                <input type="text" name="lname" id="lname"
                                                       placeholder="Enter your last name" class="form-control">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="cname" class="control-label mb-1">Company Name</label>
                                                <input type="text" name="cname" id="cname"
                                                       placeholder="Enter your company name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="email" class="control-label mb-1">Email Address</label>
                                            <div class="input-group">
                                                <input type="email" name="email" id="email"
                                                       placeholder="Enter your email address" class="form-control">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="phone" class="control-label mb-1">Phone Number</label><br>
                                                <input type="tel" id="phone" name="phone" id="phone"
                                                       class="form-control">
                                                <span id="valid-msg" class="hide text-success">Valid Number</span>
                                                <span id="error-msg" class="hide text-danger">Invalid number</span>
                                                <input type="hidden" class="form-control" id="phone2" name="phone2">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="skype" class="control-label mb-1">Skype</label>
                                            <div class="input-group">
                                                <input type="text" name="skype" id="skype"
                                                       placeholder="Enter your skype name" class="form-control">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong><i class="fas fa-file-signature"></i> Login Information</strong>
                                </div>
                                <div class="card-body card-block">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="email" class="control-label mb-1">Login</label>
                                                <input type="text" name="email" id="email"
                                                       placeholder="Enter your first name" value="Email Address"
                                                       class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong><i class="fas fa-map-marker-alt"></i> Address</strong>
                                </div>
                                <div class="card-body card-block">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="address" class="control-label mb-1">Address</label>
                                                <input type="text" name="address" id="address"
                                                       placeholder="Enter your address" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="city" class="control-label mb-1">City</label>
                                            <div class="input-group">
                                                <input type="text" name="city" id="city" placeholder="Enter your city"
                                                       class="form-control">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="country" class="control-label mb-1">Country</label>
                                            <div class="input-group">
                                                <select name="country" id="country" class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="scr"
                                                       class="control-label mb-1">State/County/Region</label>
                                                <input type="text" name="scr" id="scr"
                                                       placeholder="Enter your state/county/region"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <!--<div>
                                            <!-- country-region dropdown menu with country flag --
                                            <p>Example 3: Country-Region DropDown Menu with country flag</p>

                                            <p>
                                                Country: <select class="gds-cr gds-countryflag" country-data-region-id="gds-cr-three" ></select>

                                                Region: <select id="gds-cr-three"></select>
                                            </p>
                                        </div>
                                        </div>-->
                                        <div class="col-6">
                                            <label for="zip" class="control-label mb-1">Zip Code</label>
                                            <div class="input-group">
                                                <input type="tel" name="zip" id="zip" placeholder="Enter your zip code"
                                                       class="form-control">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer left">
                            <button type="submit" name="save" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Save Changes
                            </button>
                            <?php
                            $country = $_POST['con'];
                            if(isset ($_POST['save'])){
                                echo $country;
                            }
                            ?>
                            <button type="reset" name="reset" id="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>
                        </div>
                    </form>
                </div>

                <?php require_once('includes/foot.php'); ?>

            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->

<?php require_once('includes/footer.php'); ?>