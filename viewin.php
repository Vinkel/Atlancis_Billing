<style>
    body {
        background-color: #f4f7f9;
        margin: 10px auto;
        width: 650px;
        font-size:11px;
        color:#000;
        text-align:center;
        font-family:Arial, Helvetica, sans-serif;
    }
    .CellLabel {
        color: #000000;
        font-size: 11px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
    }
    div.inv1 {
        background: #efefef;
        padding: 0 0 0 10px;
    }
    div.inv2 {
        padding: 10px 0 0;
    }
    div.inv3 {
        padding: 0 10px 0 0;
    }
    div.inv4 {
        margin: 0 0 0 -10px;
        padding: 0 0 10px 10px;
    }
    .InvoiceAreaTitle {
        color: #000000;
        font-size: 14px;
        font-weight: bold;
    }
    .InvoiceTableColumnTitles {
        color: #000000;
        font-size: 12px;
        font-style: normal;
        font-variant: normal;
        font-weight: bold;
    }
    .ClientName {
        color: #000000;
        font-size: 18px;
        font-style: normal;
        font-variant: normal;
        font-weight: bold;
    }
    * {
        font-size: 1em;
    }
    body > table{
        background:#fff;
        padding: 10px 20px;
        border-radius: 5px 5px 0 0;
        border: 1px solid #CCCCCC;
        border-bottom:none;
    }
    body > div{
        padding:0 !important;
    }
    body > div > table{
        background:#fff;
        padding: 10px 20px;
        border-radius:0 0 5px 5px;
        border: 1px solid #CCCCCC;
        border-top:none;
    }
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
    <title>Invoice 19647-10121552</title>

</head>
<body bgcolor="#FFFFFF"><body bgcolor="#FFFFFF">

<script language="JavaScript">
    //<!--
    function show_child(parentid) {
        var i = 0;
        var child;

        while (child = document.getElementById('child_' + parentid + '_' + i)) {
            child.className = 'show';
            i++;
        }

        document.getElementById('minus[' + parentid + ']').style.display='block';
        document.getElementById('plus[' + parentid + ']').style.display='none';
        document.getElementById('total[' + parentid + ']').style.fontWeight='bold';
        return false;
    }

    function hide_child(parentid) {
        var i = 0;
        var child;

        while (child = document.getElementById('child_' + parentid + '_' + i)) {
            child.className = 'hide';
            i++;
        }

        document.getElementById('plus[' + parentid + ']').style.display='block';
        document.getElementById('minus[' + parentid + ']').style.display='none';
        document.getElementById('total[' + parentid + ']').style.fontWeight='normal';
        return false;
    }
    //-->
</script>
<table width="100%" cellpadding="10" cellspacing="0" border="0"><tr><td>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="33%"><img src="images/icon/cloud1.png" alt="Atlancis Cloud"></td>
                    <td width="33%"><p class="CellLabel"><strong>Remit to:</strong><br>
                            Profuse Solutions, INC<br />
                            20687-2 Amar Road #312<br />
                            Walnut, CA 91789
                    </td>
                    <td width="33%" align="center">
                        <table width="200" border="0" cellspacing="0" cellpadding="5">
                            <tr class="CellLabel">
                                <td><strong>Invoice Date:</strong></td>
                                <td>Aug/27/2018</td>
                            </tr>
                            <tr class="CellLabel">
                                <td><strong>Invoice Due Date:</strong></td>
                                <td>Sep/01/2018</td>
                            </tr>
                            <tr class="CellLabel">
                                <td><strong>Invoice Number:</strong></td>
                                <td>INV-19647-10121552</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="6">
                <tr>
                    <td width="60%">
                        <div class="inv1"><div class="inv2"><div class="inv3"><div class="inv4">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="0" class="CellLabel">
                                            <tr>
                                                <td valign="top" class="InvoiceAreaTitle">Customer</td>
                                                <td colspan="3" class="InvoiceTableColumnTitles">
                                                    Atlancis<br/>
                                                    Attn: Vinkel Mwiti<br/>
                                                    92 Kikuyu<br />
                                                    Kikuyu, Tharaka Nithi 00902<br />
                                                    Kenya
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><img src="/images/clearpixel.gif" width="1" height="4"></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" valign="top" class="InvoiceAreaTitle">Client Ledger</td>
                                                <td>Prior Balance</td>
                                                <td>$</td>
                                                <td align="right">0.00</td>
                                            </tr>
                                            <tr>
                                                <td>Payments / Refunds</td>
                                                <td>$</td>
                                                <td align="right">0.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><img src="/images/clearpixel.gif" width="1" height="4"></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="3" valign="top" class="InvoiceAreaTitle">Current Charges</td>
                                                <td>Service Items</td>
                                                <td>$</td>
                                                <td align="right">269.00</td>
                                            </tr>
                                            <tr>
                                                <td>Taxes</td>
                                                <td>$</td>
                                                <td align="right">0.00</td>
                                            </tr>
                                            <tr style="font-weight:bold;">
                                                <td>Invoice Total</td>
                                                <td style="border-top:1px solid #000000;">$</td>
                                                <td align="right" style="border-top:1px solid #000000;">269.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><img src="/images/clearpixel.gif" width="1" height="4"></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" valign="top" class="InvoiceAreaTitle">Account Balance</td>
                                                <td rowspan="2">as of Aug/27/2018</td>
                                                <td class="ClientName">$</td>
                                                <td align="right" class="ClientName">269.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="border-top:1px solid #000000;border-bottom:1px solid #000000;padding:0;">
                                                    <img src="/images/clearpixel.gif" width="1" height="2">
                                                </td>
                                            </tr>
                                        </table>
                                    </div></div></div></div>
                    </td>
                    <td width="40%" valign="top">
                        <div class="inv1" style="background-color:#ffffdd;"><div class="inv2"><div class="inv3"><div class="inv4">
                                        <div class="CellLabel" style="padding:3px;">This invoice has been sent manually without automatically processing a payment.<br />
                                            Please contact us if you wish to arrange an alternate method of payment.<br />
                                        </div>
                                    </div></div></div></div>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellpadding="4" cellspacing="0">
                <tr>
                    <td colspan="10" class="InvoiceAreaTitle" style="border-bottom:1px solid #cccccc;">Service Items</td>
                </tr>
                <tr class="InvoiceTableColumnTitles" bgcolor="#efefef">
                    <td>&nbsp;</td>
                    <td align="center">ID#</td>
                    <td colspan="2">Service Description</td>
                    <td align="center">Date Range</td>
                    <td align="center">Unit Price</td>
                    <td align="center">Quantity</td>
                    <td style="width:2%;">&nbsp;</td>
                    <td align="center" style="width:10%;">Total Due</td>
                    <td style="width:4%;">&nbsp;</td>
                </tr>
                <tr valign="top">
                    <td></td>
                    <td align="center">64271</td>
                    <td colspan="2">Taipei - E3-1230v6 Series Server (TPQC1)
                        <div style="margin:0 0 0 10px;">base price ($ 104.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Data Center Location: Taipei, TW (+$ 0.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Processor: E3-1230 V6 (+$ 0.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Memory: 16GB (+$ 10.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Primary Storage Drives: 1TB SATA-3 (+$ 5.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Operating System: CentOS 6 Latest Stable (+$ 0.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Control Panel: No Control Panel (+$ 0.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">R1Soft CDP Data Protection: No Data Protection (+$ 0.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Bandwidth: 10Mbps Unmetered (+$ 150.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">IP Address Allocation: /29 - 5 IPs (+$ 0.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Switch Port Speed: 100Mbps (+$ 0.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">Managed Services: Self-Managed Server Administration (+$ 0.00 / Month)</div>
                        <div style="margin:0 0 0 10px;">DDoS Protection Level: No DDoS Protection (+$ 0.00 / Month)</div></td>
                    <td align="center">Aug/27/2018 - Sep/27/2018</td>
                    <td align="center">$ 269.00</td>
                    <td align="center">1</td>
                    <td align="center">$</td>
                    <td align="right">269.00</td>
                    <td align="center" style="padding-top:2px;"><i class="tip_icon uf-icon-help" href="#" title="Why was this billed?" onclick="return toggle_tip('0',event);"></i></td>
                </tr>
                <tr>
                    <td colspan="10" align="center" style="padding:0;font-size:5px;">&nbsp;</td>
                </tr>
                <tr bgcolor="#efefef">
                    <td colspan="7" align="center"><div align="right" class="InvoiceAreaTitle">Invoice Total:</div></td>
                    <td align="center" class="InvoiceAreaTitle">$</td>
                    <td align="right" class="InvoiceAreaTitle">269.00</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <br><br>
            <table width="85%" border="0" align="center" cellpadding="5" cellspacing="0">
                <tr align="center">
                    <td colspan="6" class="InvoiceTableColumnTitles">Aging Invoice Balance as of Aug/27/2018</td>
                </tr>
                <tr align="center" class="CellLabel">
                    <td style="border-bottom:1px solid #cccccc;"><strong>Current</strong></td>
                    <td style="border-bottom:1px solid #cccccc;"><strong>0-30 Days</strong></td>
                    <td style="border-bottom:1px solid #cccccc;"><strong>31-60 Days</strong></td>
                    <td style="border-bottom:1px solid #cccccc;"><strong>61-90 Days</strong></td>
                    <td style="border-bottom:1px solid #cccccc;"><strong>91-120 Days</strong></td>
                    <td style="border-bottom:1px solid #cccccc;"><strong>121+ Days</strong></td>
                </tr>
                <tr align="center" class="CellLabel">
                    <td>$ 269.00</td>
                    <td>$ 0.00</td>
                    <td>$ 0.00</td>
                    <td>$ 0.00</td>
                    <td>$ 0.00</td>
                    <td>$ 0.00</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div style="padding:5px;">
    <table width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr>
            <td colspan="7" class="InvoiceAreaTitle" style="border-bottom:1px solid #cccccc;padding:5px;">Payment &amp; Credit History</td>
        </tr>
        <tr class="InvoiceTableColumnTitles" align="center" bgcolor="#efefef">
            <td width="4%">&nbsp;</td>
            <td width="16%">Date</td>
            <td width="25%">Type</td>
            <td width="25%">Details</td>
            <td width="15%" colspan="2">Amount</td>
            <td width="15%">&nbsp;</td>
        </tr>
        <tr bgcolor="#ffffff" align="center">
            <td>
                <div id="plus[pay_177596]" style="display:block;"><a style="text-decoration:none;" href="#" onclick="return show_child('pay_177596');"><font size="1" color="#003366">[+]</font></a></div>
                <div id="minus[pay_177596]" style="display:none;"><a style="text-decoration:none;" href="#" onclick="return hide_child('pay_177596');"><font size="1" color="#003366">[-]</font></a></div>
            </td>
            <td>Aug/28/2018</td>
            <td align="left">Account credit for Service Deactivation</td>
            <td>Service Deactivation<br />Credit ID: 36731</td>
            <td>$</td>
            <td align="right" id="total[pay_177596]">(269.00)</td>
            <td align="right">&nbsp;</td>
        </tr>
        <tr id="child_pay_177596_0" bgcolor="#ffffff" class="hide" align="center">
            <td class="CellText">&nbsp;</td>
            <td class="CellText">64271</td>
            <td class="CellText" colspan="2" align="left">Taipei - E3-1230v6 Series Server (TPQC1)</td>
            <td class="CellText">$</td>
            <td class="CellText" align="right">(269.00)</td>
            <td class="CellText">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" align="right" style="border-top:1px solid #cccccc;"><strong>Total Payments for Invoice</strong></td>
            <td align="center" style="border-top:1px solid #cccccc;"><strong>$</strong></td>
            <td align="right" style="border-top:1px solid #cccccc;"><strong>(269.00)</strong></td>
            <td style="border-top:1px solid #cccccc;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" class="InvoiceAreaTitle" style="border-bottom:1px solid #cccccc;padding:15px 5px 5px;">Refund History</td>
        </tr>
        <tr class="InvoiceTableColumnTitles" align="center" bgcolor="#efefef">
            <td>&nbsp;</td>
            <td>Date</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2">Amount</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" align="right" style="border-top:1px solid #cccccc;"><strong>Total Refunds for Invoice</strong></td>
            <td align="center" style="border-top:1px solid #cccccc;"><strong>$</strong></td>
            <td align="right" style="border-top:1px solid #cccccc;"><strong>0.00</strong></td>
            <td style="border-top:1px solid #cccccc;">&nbsp;</td>
        </tr>
    </table>
</div>
</body>
</html>