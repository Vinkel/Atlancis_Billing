<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:56 PM
 */

require_once('includes/header.php');

?>

    <!-- MAIN CONTENT-->
    <div class="main-content">

    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <!-- Resource Usage -->
            <div class="row">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-1"><i class="fas fa-shopping-cart"></i> Orders</h2>
                    </div>
                </div>

                <div class="col-md-12">

                </div>
            </div>


            <div class="box">
                <!--<div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                </div>-->
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-striped dataTable">
                        <thead>
                        <tr>
                            <th>Invoice No.</th>
                            <th>Invoice Subject</th>
                            <th>Client</th>
                            <th>VAT No.</th>
                            <th>Created Date</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>0000001</td>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 4.0
                            </td>
                            <td>Win 95+</td>
                            <td> 4</td>
                            <td>X</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000002</td>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 5.0
                            </td>
                            <td>Win 95+</td>
                            <td>5</td>
                            <td>C</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000003</td>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 5.5
                            </td>
                            <td>Win 95+</td>
                            <td>5.5</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000004</td>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 6
                            </td>
                            <td>Win 98+</td>
                            <td>6</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000005</td>
                            <td>Trident</td>
                            <td>Internet Explorer 7</td>
                            <td>Win XP SP2+</td>
                            <td>7</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000006</td>
                            <td>Trident</td>
                            <td>AOL browser (AOL desktop)</td>
                            <td>Win XP</td>
                            <td>6</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000007</td>
                            <td>Gecko</td>
                            <td>Firefox 1.0</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td>1.7</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000008</td>
                            <td>Gecko</td>
                            <td>Firefox 1.5</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td>1.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000009</td>
                            <td>Gecko</td>
                            <td>Firefox 2.0</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td>1.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000010</td>
                            <td>Gecko</td>
                            <td>Firefox 3.0</td>
                            <td>Win 2k+ / OSX.3+</td>
                            <td>1.9</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000011</td>
                            <td>Gecko</td>
                            <td>Camino 1.0</td>
                            <td>OSX.2+</td>
                            <td>1.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000012</td>
                            <td>Gecko</td>
                            <td>Camino 1.5</td>
                            <td>OSX.3+</td>
                            <td>1.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000013</td>
                            <td>Gecko</td>
                            <td>Netscape 7.2</td>
                            <td>Win 95+ / Mac OS 8.6-9.2</td>
                            <td>1.7</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000014</td>
                            <td>Gecko</td>
                            <td>Netscape Browser 8</td>
                            <td>Win 98SE+</td>
                            <td>1.7</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000015</td>
                            <td>Gecko</td>
                            <td>Netscape Navigator 9</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td>1.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000016</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.0</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td>1</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000017</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.1</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td>1.1</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000018</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.2</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td>1.2</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000019</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.3</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td>1.3</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000020</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.4</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td>1.4</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000021</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.5</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td>1.5</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000022</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.6</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td>1.6</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000023</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.7</td>
                            <td>Win 98+ / OSX.1+</td>
                            <td>1.7</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000024</td>
                            <td>Gecko</td>
                            <td>Mozilla 1.8</td>
                            <td>Win 98+ / OSX.1+</td>
                            <td>1.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000025</td>
                            <td>Gecko</td>
                            <td>Seamonkey 1.1</td>
                            <td>Win 98+ / OSX.2+</td>
                            <td>1.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000026</td>
                            <td>Gecko</td>
                            <td>Epiphany 2.20</td>
                            <td>Gnome</td>
                            <td>1.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000027</td>
                            <td>Webkit</td>
                            <td>Safari 1.2</td>
                            <td>OSX.3</td>
                            <td>125.5</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000028</td>
                            <td>Webkit</td>
                            <td>Safari 1.3</td>
                            <td>OSX.3</td>
                            <td>312.8</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000029</td>
                            <td>Webkit</td>
                            <td>Safari 2.0</td>
                            <td>OSX.4+</td>
                            <td>419.3</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000030</td>
                            <td>Webkit</td>
                            <td>Safari 3.0</td>
                            <td>OSX.4+</td>
                            <td>522.1</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000031</td>
                            <td>Webkit</td>
                            <td>OmniWeb 5.5</td>
                            <td>OSX.4+</td>
                            <td>420</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000032</td>
                            <td>Webkit</td>
                            <td>iPod Touch / iPhone</td>
                            <td>iPod</td>
                            <td>420.1</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000033</td>
                            <td>Webkit</td>
                            <td>S60</td>
                            <td>S60</td>
                            <td>413</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000034</td>
                            <td>Presto</td>
                            <td>Opera 7.0</td>
                            <td>Win 95+ / OSX.1+</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000035</td>
                            <td>Presto</td>
                            <td>Opera 7.5</td>
                            <td>Win 95+ / OSX.2+</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000036</td>
                            <td>Presto</td>
                            <td>Opera 8.0</td>
                            <td>Win 95+ / OSX.2+</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000037</td>
                            <td>Presto</td>
                            <td>Opera 8.5</td>
                            <td>Win 95+ / OSX.2+</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000038</td>
                            <td>Presto</td>
                            <td>Opera 9.0</td>
                            <td>Win 95+ / OSX.3+</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000039</td>
                            <td>Presto</td>
                            <td>Opera 9.2</td>
                            <td>Win 88+ / OSX.3+</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000040</td>
                            <td>Presto</td>
                            <td>Opera 9.5</td>
                            <td>Win 88+ / OSX.3+</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000041</td>
                            <td>Presto</td>
                            <td>Opera for Wii</td>
                            <td>Wii</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000042</td>
                            <td>Presto</td>
                            <td>Nokia N800</td>
                            <td>N800</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000043</td>
                            <td>Presto</td>
                            <td>Nintendo DS browser</td>
                            <td>Nintendo DS</td>
                            <td>8.5</td>
                            <td>C/A<sup>1</sup></td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000044</td>
                            <td>KHTML</td>
                            <td>Konqureror 3.1</td>
                            <td>KDE 3.1</td>
                            <td>3.1</td>
                            <td>C</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000045</td>
                            <td>KHTML</td>
                            <td>Konqureror 3.3</td>
                            <td>KDE 3.3</td>
                            <td>3.3</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000046</td>
                            <td>KHTML</td>
                            <td>Konqureror 3.5</td>
                            <td>KDE 3.5</td>
                            <td>3.5</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000047</td>
                            <td>Tasman</td>
                            <td>Internet Explorer 4.5</td>
                            <td>Mac OS 8-9</td>
                            <td>-</td>
                            <td>X</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000048</td>
                            <td>Tasman</td>
                            <td>Internet Explorer 5.1</td>
                            <td>Mac OS 7.6-9</td>
                            <td>1</td>
                            <td>C</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000049</td>
                            <td>Tasman</td>
                            <td>Internet Explorer 5.2</td>
                            <td>Mac OS 8-X</td>
                            <td>1</td>
                            <td>C</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000050</td>
                            <td>Misc</td>
                            <td>NetFront 3.1</td>
                            <td>Embedded devices</td>
                            <td>-</td>
                            <td>C</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000051</td>
                            <td>Misc</td>
                            <td>NetFront 3.4</td>
                            <td>Embedded devices</td>
                            <td>-</td>
                            <td>A</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000052</td>
                            <td>Misc</td>
                            <td>Dillo 0.8</td>
                            <td>Embedded devices</td>
                            <td>-</td>
                            <td>X</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000053</td>
                            <td>Misc</td>
                            <td>Links</td>
                            <td>Text only</td>
                            <td>-</td>
                            <td>X</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000054</td>
                            <td>Misc</td>
                            <td>Lynx</td>
                            <td>Text only</td>
                            <td>-</td>
                            <td>X</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000055</td>
                            <td>Misc</td>
                            <td>IE Mobile</td>
                            <td>Windows Mobile 6</td>
                            <td>-</td>
                            <td>C</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000056</td>
                            <td>Misc</td>
                            <td>PSP browser</td>
                            <td>PSP</td>
                            <td>-</td>
                            <td>C</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>0000057</td>
                            <td>Other browsers</td>
                            <td>All others</td>
                            <td>-</td>
                            <td>-</td>
                            <td>U</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="viewod.php"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Invoice No.</th>
                            <th>Invoice Subject</th>
                            <th>Client</th>
                            <th>VAT No.</th>
                            <th>Created Date</th>
                            <th>Status</th>
                            <th>Price</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <?php require_once('includes/foot.php'); ?>

        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->

<?php require_once('includes/footer.php'); ?>