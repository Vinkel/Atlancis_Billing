<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:56 PM
 */

require_once('includes/header.php');

?>

    <!-- MAIN CONTENT-->
    <div class="main-content">

    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <!-- Resource Usage -->
            <div class="row">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-1"><i class="fas fa-file-invoice"></i> Invoices</h2>
                    </div>
                </div>

                <div class="col-md-12">

                </div>
            </div>


            <div class="box">
                <!--<div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                </div>-->
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped dataTable">
                        <thead>
                        <tr>
                            <th>Invoice No.</th>
                            <th>Invoice Subject</th>
                            <th>Client</th>
                            <th>VAT No.</th>
                            <th>CSS grade</th>
                            <th>CSS grade</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 4.0
                            </td>
                            <td>Win 95+</td>
                            <td> 4</td>
                            <td> 4</td>
                            <td>X</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 5.0
                            </td>
                            <td>Win 95+</td>
                            <td>5</td>
                            <td>C</td>
                            <td>C</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 5.5
                            </td>
                            <td>Win 95+</td>
                            <td>5.5</td>
                            <td>5.5</td>
                            <td>A</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 6
                            </td>
                            <td>Win 98+</td>
                            <td>6</td>
                            <td>A</td>
                            <td>A</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Trident</td>
                            <td>Internet Explorer 7</td>
                            <td>Win XP SP2+</td>
                            <td>7</td>
                            <td>A</td>
                            <td>A</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Trident</td>
                            <td>AOL browser (AOL desktop)</td>
                            <td>Win XP</td>
                            <td>6</td>
                            <td>A</td>
                            <td>A</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th>Engine version</th>
                            <th>CSS grade</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <?php require_once('includes/foot.php'); ?>

        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->

<?php require_once('includes/footer.php'); ?>