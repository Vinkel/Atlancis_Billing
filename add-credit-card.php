<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:56 PM
 */

require_once('includes/header.php');

?>

    <!-- MAIN CONTENT-->
    <div class="main-content">

        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <!-- Resource Usage -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1"><i class="far fa-credit-card"></i> Add New Credit Card Details</h2>
                            <!--<button class="au-btn au-btn-icon au-btn&#45;&#45;blue">
                                <i class="zmdi zmdi-plus"></i>add item</button>-->
                        </div>
                    </div>

                    <form id="card" method="post" action="add-credit-card.php">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong><i class="fas fa-credit-card"></i> Card Information</strong>
                                </div>
                                <div class="card-body card-block">
                                    <div class="row">
                                        <div class="col-6 offset-3">
                                            <div class="form-group text-center">
                                                <label for="cc-no" class="control-label mb-1">Card Number</label>
                                                <input type="text" name="fname" id="fname"
                                                       placeholder="Enter your card number" class="form-control text-center">
                                            </div>
                                        </div>

                                        <div class="col-7 offset-2">
                                            <div class="label" style="padding-left: 210px;"><strong>Expiry Date:</strong></div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="form-group text-center">
                                                        <label for="month" class="control-label mb-1">Month</label>
                                                        <select type="text" name="month" id="month"
                                                                class="form-control">
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">August</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group text-center">
                                                        <label for="month" class="control-label mb-1">Year</label>
                                                        <select type="text" name="year" id="year"
                                                                class="form-control">
                                                            <option value="2018">2018</option>
                                                            <option value="2019">2019</option>
                                                            <option value="2020">2020</option>
                                                            <option value="2021">2021</option>
                                                            <option value="2022">2022</option>
                                                            <option value="2023">2023</option>
                                                            <option value="2024">2024</option>
                                                            <option value="2025">2025</option>
                                                            <option value="2026">2026</option>
                                                            <option value="2027">2027</option>
                                                            <option value="2028">2028</option>
                                                            <option value="2029">2029</option>
                                                            <option value="2030">2030</option>
                                                            <option value="2031">2031</option>
                                                            <option value="2032">2032</option>
                                                            <option value="2033">2033</option>
                                                            <option value="2034">2034</option>
                                                            <option value="2035">2035</option>
                                                            <option value="2036">2036</option>
                                                            <option value="2037">2037</option>
                                                            <option value="2038">2038</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group text-center">
                                                        <label for="cvv" class="control-label mb-1">CVV/CVC2</label>
                                                        <input type="password" name="cvv" id="cvv"
                                                               placeholder="Enter your CVV" class="form-control text-center">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong><i class="fas fa-user"></i> Credit Card Account Holder's Information</strong>
                                </div>
                                <div class="card-body card-block">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="fname" class="control-label mb-1">First Name</label>
                                                <input type="text" name="fname" id="fname"
                                                       placeholder="Enter your first name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="lname" class="control-label mb-1">Last Name</label>
                                            <div class="input-group">
                                                <input type="text" name="lname" id="lname"
                                                       placeholder="Enter your last name" class="form-control">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="cname" class="control-label mb-1">Company Name</label>
                                                <input type="text" name="cname" id="cname"
                                                       placeholder="Enter your company name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="email" class="control-label mb-1">Email Address</label>
                                            <div class="input-group">
                                                <input type="email" name="email" id="email"
                                                       placeholder="Enter your email address" class="form-control">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="phone" class="control-label mb-1">Phone Number</label><br>
                                                <input type="tel" id="phone" name="phone" id="phone"
                                                       class="form-control">
                                                <span id="valid-msg" class="hide text-success">Valid Number</span>
                                                <span id="error-msg" class="hide text-danger">Invalid number</span>
                                                <input type="hidden" class="form-control" id="phone2" name="phone2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong><i class="fas fa-map-marker-alt"></i> Credit Card Billing Address</strong>
                                </div>
                                <div class="card-body card-block">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="cc-exp" class="control-label mb-1">Address</label>
                                                <input type="text" name="address" id="address"
                                                       placeholder="Enter your address" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="x_card_code" class="control-label mb-1">City</label>
                                            <div class="input-group">
                                                <input type="text" name="city" id="city" placeholder="Enter your city"
                                                       class="form-control">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="x_card_code" class="control-label mb-1">Country</label>
                                            <div class="input-group">
                                                <select class="form-control"></select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="cc-exp"
                                                       class="control-label mb-1">State/County/Region</label>
                                                <input type="text" name="scr" id="scr"
                                                       placeholder="Enter your state/county/region"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <!--<div>
                                            <!-- country-region dropdown menu with country flag --
                                            <p>Example 3: Country-Region DropDown Menu with country flag</p>

                                            <p>
                                                Country: <select class="gds-cr gds-countryflag" country-data-region-id="gds-cr-three" ></select>

                                                Region: <select id="gds-cr-three"></select>
                                            </p>
                                        </div>
                                        </div>-->
                                        <div class="col-6">
                                            <label for="x_card_code" class="control-label mb-1">Zip Code</label>
                                            <div class="input-group">
                                                <input type="tel" name="zip" id="zip" placeholder="Enter your zip code"
                                                       class="form-control">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer left">
                            <button type="submit" name="save" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Save Changes
                            </button>
                            <?php
                            $country = $_POST['con'];
                            if(isset ($_POST['save'])){
                                echo $country;
                            }
                            ?>
                            <button type="reset" name="reset" id="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>
                        </div>
                    </form>
                </div>

                <?php require_once('includes/foot.php'); ?>

            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->

<?php require_once('includes/footer.php'); ?>