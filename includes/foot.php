<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/28/18
 * Time: 3:36 PM
 */
?>

<!-- Footer -->
<div class="row">
    <div class="col-md-12">
        <div class="copyright">
            <p>Copyright © 2018 Atlancis Cloud. All rights reserved. <a
                    href="https://atlancis.com">Atlancis Technologies Limited</a>.</p>
        </div>
    </div>
</div><!-- End Footer -->