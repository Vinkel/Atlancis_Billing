<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:57 PM
 */
?>

</div>
</div>
<script src="https://apis.google.com/js/api.js" type="text/javascript"></script>
<!-- Jquery JS-->
<script src="vendor/jquery-3.2.1.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap JS-->
<script src="vendor/bootstrap-4.1/popper.min.js"></script>
<script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="vendor/slick/slick.min.js">
</script>
<script src="vendor/wow/wow.min.js"></script>
<script src="vendor/animsition/animsition.min.js"></script>
<script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="vendor/circle-progress/circle-progress.min.js"></script>
<script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="vendor/chartjs/Chart.bundle.min.js"></script>
<script src="vendor/select2/select2.min.js">
</script>

<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net/js/dataTables.bootstrap4.min.js"></script>

<!-- Int-Phone-Number -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.js"></script>
<script src="js/index.js"></script>
<script>

    $("#phone").mouseleave(function () {
        var tel = $("#phone").intlTelInput("getNumber");
        $("#phone2").val(tel);
        // alert(tel);
    });

    $("#form-contact-submit").click(function () {
        if ($("#phone2").val() == '') {
            window.location.hash = "#account";
            return false;
        }

    });

</script>

<!-- Country Picker -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/country-region-dropdown-menu/1.2.1/geodatasource-cr.min.js"></script>
<script>
    $(document).ready(function () {
        $("a.learn_more").click(function () {
            window.open($(this).attr("href"));
            return false;
        });
    });
</script>

<!-- Main JS-->
<script src="js/main.js"></script>
<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable();
        $('#example2').DataTable();
        //$('#example3').DataTable();
    });
</script>
<script>

    /*$(document).ready(function () {

        $('#example3').DataTable();

        $.getJSON("http://localhost/api/tsconfig.json" +$(this).val(),
            function (j) {
                console.log(j);
                //alert(j);
                for (var i = 0; i < j.length; i++) {
                    var no = j[i]['no'];
                    var subject = j[i]['subject'];
                    var client = j[i]['client'];
                    var vat = j[i]['vat'];
                    var date = j[i]['date'];
                    var status = j[i]['status'];
                    var price = j[i]['price'];
                    var action = j[i]['action'];

                    //alert(i);

                    $("#example3").append("<tr><td class='text-center'>" + no
                        + "</td><td class='text-center' >" + subject +
                        "</td><td class='text-center' >" + client +
                        "</td><td class='text-center'>" + vat + "</td><td class='text-center'>" + date + "</td><td class='text-center'>" + status + "</td><td class='text-center'>" + price + "</td><td class='text-center'>" + action + "</td></tr>");
                }

            });
    });*/

    /*$(document).ready(function () {
        $.getJSON("http://localhost/api/tsconfig.json",
            function(j){
                console.log(j);
                for (var i = 0; i < j.length; i++) {
                    var no = j[i]['no'];
                    var subject = j[i]['subject'];
                    var client = j[i]['client'];
                    var vat = j[i]['vat'];
                    var date = j[i]['date'];
                    var status = j[i]['status'];
                    var price = j[i]['price'];
                    var action = j[i]['action'];

                    $("#example3").append("<tr><td class='text-center'>" + no
                        + "</td><td class='text-center'>" + subject +
                        "</td><td class='text-center'>" + client + "</td><td class='text-center'>" + vat +
                        "</td><td class='text-center'>" + date + "</td><td class='text-center'>" + status +
                        "</td><td class='text-center'>" + price +
                        "</td><td class='text-center'>" + action + "</td></tr>");

                }


            });
    });*/

    $(document).ready(function () {

        $.ajax({
            'url': "http://localhost/api/tsconfig.json",
            'method': "GET",
            'contentType': 'application/json'
        }).done(function (data) {
            $('#example3').dataTable({
                "aaData": data,
                "columns": [
                    {"data": "no"},
                    {"data": "subject"},
                    {"data": "client"},
                    {"data": "vat"},
                    {"data": "date"},
                    {"data": "status"},
                    {"data": "price"},
                    {
                        mRender: function (data, type, full) {
                            return '<td class="text-center"> <ul class="icons-list"> <li class="dropdown"> <a href="#" class="" data-toggle="dropdown"> <i class="icon-menu9"></i> </a> <ul class="dropdown-menu dropdown-menu-right"> <li><a onclick="pdf();" href="#"><i class="icon-file-pdf"></i> Pdf</a></li> </ul> </td>';
                        }
                    }
                ]
            });
        });
    });

</script>
</body>
</html>
<!-- end document-->