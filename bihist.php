<?php
/**
 * Created by PhpStorm.
 * User: vinkel
 * Date: 8/27/18
 * Time: 4:56 PM
 */

require_once('includes/header.php');

?>

    <!-- MAIN CONTENT-->
    <div class="main-content">

    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <!-- Resource Usage -->
            <div class="row">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h2 class="title-1"><i class="fas fa-history"></i> Billing History</h2>
                    </div>
                </div>

                <div class="col-md-12">

                </div>
            </div>


            <div class="box">
                <!--<div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                </div>-->
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example3" class="bihist table table-bordered table-hover table-striped dataTable">
                        <thead>
                        <tr>
                            <th class="text-center">Invoice No.</th>
                            <th class="text-center">Invoice Subject</th>
                            <th class="text-center">Client</th>
                            <th class="text-center">VAT No.</th>
                            <th class="text-center">Created Date</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        <!--<tr>
                            <td>0000001</td>
                            <td>Trident</td>
                            <td>Internet
                                Explorer 4.0
                            </td>
                            <td>Win 95+</td>
                            <td> 4</td>
                            <td>X</td>
                            <td>$1000</td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#"><i class="fas fa-search"></i> View</a></li>
                                            <li><a href="#"><i class="icon-file-pdf"></i> Pdf</a></li>
                                            <li><a href="#"><i class="icon-file-excel"></i> Excel</a></li>
                                            <li><a href="#"><i class="icon-file-word"></i> Doc</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>-->
                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="text-center">Invoice No.</th>
                            <th class="text-center">Invoice Subject</th>
                            <th class="text-center">Client</th>
                            <th class="text-center">VAT No.</th>
                            <th class="text-center">Created Date</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>

            <?php require_once('includes/foot.php'); ?>

        </div>
    </div>
    <!-- END MAIN CONTENT-->
    <!-- END PAGE CONTAINER-->

<?php require_once('includes/footer.php'); ?>